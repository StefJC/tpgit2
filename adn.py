#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    documentation here
"""

__author__ = 'Stéphane Mancini'



def is_valid(adn_str):
	nucs = ['a', 'c', 'g', 't', 'A', 'C', 'G', 'T']
	for i in adn_str:
		if i not in nucs:
			return False
	return True


def get_valid_adn(prompt='chaîne : '):
	adn_str = input(prompt)
	while is_valid(adn_str) is False:
		adn_str = input(prompt)
	return(adn_str)

get_valid_adn()

